# dune-common
# releases/2.6 # 1b45bfb24873f80bfc7b419c04dea79f93aef1d7 # 2019-03-19 10:11:58 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.6
git reset --hard 1b45bfb24873f80bfc7b419c04dea79f93aef1d7
cd ..

# dune-geometry
# releases/2.6 # 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38 # 2018-12-06 22:56:21 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.6
git reset --hard 1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38
cd ..

# dune-grid
# releases/2.6 # 76f18471498824d49a6cecbfba520b221d9f79ca # 2018-12-10 14:35:11 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.6
git reset --hard 76f18471498824d49a6cecbfba520b221d9f79ca
cd ..

# dune-localfunctions
# releases/2.6 # ee794bfdfa3d4f674664b4155b6b2df36649435f # 2018-12-06 23:40:32 +0000 # Steffen Müthing
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.6
git reset --hard ee794bfdfa3d4f674664b4155b6b2df36649435f
cd ..

# dune-istl
# releases/2.6 # 9698e497743654b6a03c219b2bdfc27b62a7e0b3 # 2018-12-14 10:00:30 +0000 # Jö Fahlke
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.6
git reset --hard 9698e497743654b6a03c219b2bdfc27b62a7e0b3
cd ..

# dune-foamgrid
# releases/2.6 # 20f7851cac039627402419db6cb5850b4485c871 # 2019-05-01 18:45:26 +0000 # Timo Koch
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout releases/2.6
git reset --hard 20f7851cac039627402419db6cb5850b4485c871
cd ..

# dune-alugrid
# releases/2.6 # c0851a92b3af8d93a75f798de3d34f65cc895341 # 2018-07-25 11:01:08 +0000 # Martin Alkämper
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
cd dune-alugrid
git checkout releases/2.6
git reset --hard c0851a92b3af8d93a75f798de3d34f65cc895341
cd ..

# dune-subgrid
# releases/2.6-1 # 6ef4ab8198ad707af9fc7bacec279d3c7c53d4c5 # 2018-08-02 10:00:58 +0200 # Jonathan Youett
git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git
cd dune-subgrid
git checkout releases/2.6-1
git reset --hard 6ef4ab8198ad707af9fc7bacec279d3c7c53d4c5
cd ..

# dumux
# master # 1e430ae6d9188ce1dbdc9d36aa8ef59c40a41127 # 2018-10-02 17:53:00 # Simon Emmert
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout master
git reset --hard 1e430ae6d9188ce1dbdc9d36aa8ef59c40a41127
cd ..
