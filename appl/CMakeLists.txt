dune_symlink_to_source_files(FILES salinization_4cm.input salinization_16cm.input)

add_executable(salinization salinization.cc)

target_compile_definitions(salinization PRIVATE TYPETAG=DissolutionBox)
#target_compile_definitions(salinization PRIVATE TYPETAG=DissolutionCCTpfa)

set(CMAKE_BUILD_TYPE Release)


