This module contains the code used to generate the results presented in

Shokri‐Kuehni, S. M. S., Raaijmakers, B., Kurz, T., Or, D., Helmig, R., & Shokri, N. ( 2020). [Water table depth and soil salinization: From pore‐scale processes to field‐scale responses](https://doi.org/10.1029/2019WR026707). Water Resources Research

Installation
============

To install this module and its dependencies create a new
directory and clone this module:

```
mkdir New_Folder && cd New_folder
git clone https://git.iws.uni-stuttgart.de/dumux-pub/shokrikuehni2020a.git
```

After that, execute the file [installShokriKuehni2020.sh](https://git.iws.uni-stuttgart.de/dumux-pub/shokrikuehni2020a/blob/master/installShokriKuehni2020.sh)

```
chmod +x shokrikuehni2020a/installShokriKuehni2020.sh
./shokrikuehni2020a/installShokriKuehni2020.sh
```

This should automatically download all necessary modules and check out the correct versions.

Finally, run

```
./dune-common/bin/dunecontrol --opts=dumux/optim.opts all
```

Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir New_Folder
cd New_Folder
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/shokrikuehni2020a/-/raw/master/docker_shokrikuehni2020.sh
```

Open the Docker Container
```bash
bash docker_shokrikuehni2020.sh open
```

Application
============

To run the simulation go to the following folder and compile the programm.
```
cd shokrikuehni2020a/build-cmake/appl
make salinization
```

Then run the simulation with the respective input files.

```
./salinization salinization_4cm.input
./salinization salinization_16cm.input
```
